package com.lab;

import com.lab.library.Library;
import com.lab.library.Record;

import java.io.*;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        Library lib = new Library();
        Scanner sc = new Scanner(System.in);
        int option;
        while (true) {
            System.out.print("\n\n1 - Sort all books by year\n" +
                    "2 - Create address list for users, took more than 2 books\n" +
                    "3 - Amount of users have ordered a book with a given author name\n" +
                    "4 - The largest number of books taken by a library reader.\n" +
                    "5 - Make a mailing to two groups\n" +
                    "6 - Create a list of debtors for today\n" +
                    "7 - Create library\n" +
                    "8 - Serialize library\n" +
                    "9 - Init by deserialization library\n" +
                    "Another digit - Exit\n" +
                    "Enter your option: ");
            option = sc.nextInt();

            if (option == 1) {
                lib.task1();

            } else if (option == 2) {
                lib.task2();

            } else if (option == 3) {
                lib.task3();

            } else if (option == 4) {
                lib.task4();

            } else if (option == 5) {
                lib.task5();

            } else if (option == 6) {
                lib.task6();

            } else if (option == 7) {
                lib.readData();

            } else if (option == 8) {
                try (ObjectOutputStream oos = new ObjectOutputStream(
                        new FileOutputStream("ser.file"))) {
                    oos.writeObject(lib);
                    System.out.println("Library was written!");
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }

            } else if (option == 9) {
                try (ObjectInputStream ois = new ObjectInputStream(
                        new FileInputStream("ser.file"))) {
                    lib = (Library) ois.readObject();
                    System.out.println("Library was read!");
                } catch (IOException | ClassNotFoundException e) {
                    System.err.println(e.getMessage());
                }

            } else {
                break;
            }
//        System.out.println(lib.getSubscriptions());
//        System.out.println("--------------");
//        lib.task6();
        }
    }

}
