package com.lab.library;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Library implements Serializable {
    private String booksPath = "books.json";
    private String subscriptionsPath = "subscriptions.json";
    private List<Book> books = new ArrayList<>();
    private List<Subscription> subscriptions = new ArrayList<>();
    String pattern = "dd-MM-yyyy";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    public List<Book> getBooks()  {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public void readData() {
        ObjectMapper objectMapper = new ObjectMapper();
        ClassLoader classLoader = Library.class.getClassLoader();

        try {
            URL resource = classLoader.getResource(booksPath);
            books = objectMapper.readValue(
                    resource,
                    new TypeReference<>() {
                    });
            resource = classLoader.getResource(subscriptionsPath);
            subscriptions = objectMapper.readValue(
                    resource,
                    new TypeReference<>() {
                    });
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }

    public void task1() {
        books
        .stream()
        .sorted(Comparator.comparingInt(Book::getYear))
        .forEach(System.out::println);
    }

    public void task2() {
                subscriptions
                .stream()
                .filter(x -> x.getRecords().size() > 2)
                .map(Subscription::getEmail)
                .forEach(System.out::println);

        //System.out.println(subscriptions.get(1).getRecords().stream().filter(x->x.getBook().getAuthor().equals("Mariana")).count());
    }

    public void task3() {
        long result = subscriptions
                .stream()
                .filter(x -> x.getRecords().stream().anyMatch(book -> book.getBook().getAuthor().equals("Rosie"))).count();

        System.out.println(result);
    }

    public void task4() {
        System.out.println(subscriptions.stream().map(x->x.getRecords().size()).max(Integer::compareTo));
    }

    public void task5() {
        subscriptions
                .stream()
                .filter(x -> x.getRecords().size() >= 2)
                .forEach(x->x.getRecords()

                        .forEach(record -> System.out.println(
                                x.getName() + " " + x.getSurname() + ", return " + record.getBook().getName() + " before "
                                        + simpleDateFormat.format(record.getPlannedReturnDate()))));

        subscriptions
                .stream()
                .filter(x -> x.getRecords().size() < 2)
                .forEach(reader -> System.out.println(
                        reader.getName() + " " + reader.getSurname() + " we have a lot of books! For example: " + getBooks().subList(0, 2)));
    }

    public void task6() {
//        subscriptions.stream().map(Subscription::getRecords).flatMap(Collection::stream)
//                .filter(record->new Date().after(record.getPlannedReturnDate()))
//                .forEach(record -> System.out.println();
//
//        subscriptions.stream()
//                .flatMap(subscription -> subscription.getRecords().stream())
//                .filter(record -> new Date().after(record.getPlannedReturnDate()))
//                .collect(Collectors.toMap(record -> record, Record::toString)).forEach((key, value) -> {
//                    System.out.println(key);
//                    System.out.println(value);});

        subscriptions.stream().forEach(a -> {
            System.out.println(a.getName() + " " + a.getSurname());
            a.getRecords().stream().filter(b -> TimeUnit.DAYS.convert(Math.abs(new Date().getTime() - b.getPlannedReturnDate().getTime()), TimeUnit.MILLISECONDS)> 0).forEach(System.out::println);
        });

    }


}
