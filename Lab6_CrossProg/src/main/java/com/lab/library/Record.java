package com.lab.library;

import java.io.Serializable;
import java.util.Date;

public class Record implements Serializable {
    Book book;
    private Date receivingDate;
    private Date plannedReturnDate;
    private Date actualReturnDate;

    public Record(){}

    public Record(Book book, Date receivingDate, Date plannedReturnDate, Date actualReturnDate) {
        this.book = book;
        this.receivingDate = receivingDate;
        this.plannedReturnDate = plannedReturnDate;
        this.actualReturnDate = actualReturnDate;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Date getReceivingDate() {
        return receivingDate;
    }

    public void setReceivingDate(Date receivingDate) {
        this.receivingDate = receivingDate;
    }

    public Date getPlannedReturnDate() {
        return plannedReturnDate;
    }

    public void setPlannedReturnDate(Date plannedReturnDate) {
        this.plannedReturnDate = plannedReturnDate;
    }

    public Date getActualReturnDate() {
        return actualReturnDate;
    }

    public void setActualReturnDate(Date actualReturnDate) {
        this.actualReturnDate = actualReturnDate;
    }

    @Override
    public String toString() {
        return "Record{" +
                "book=" + book +
                ", receivingDate=" + receivingDate +
                ", plannedReturnDate=" + plannedReturnDate +
                ", actualReturnDate=" + actualReturnDate +
                "}";
    }
}
