import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Monitor{

    private List<Thread> threadList = new ArrayList<>();
    private List<String> statesList = new ArrayList<>();
    private long startTime;

    public void addThread(Thread thread){
        threadList.add(thread);
    }

    public void startTime() {
        startTime = System.currentTimeMillis();
    }

    public void logTime(JTextArea timeTextField) {
        long timeSpent = System.currentTimeMillis() - startTime;
        timeTextField.setText(timeSpent + " ms");
        System.out.println("time: " + timeSpent + "ms");
    }

    public void run(JTextArea textArea) {
        while (threadList.stream().anyMatch(t -> t.getState() != Thread.State.TERMINATED)){
            for (Thread thread : threadList){
                System.out.println(thread.getName() + ": " + thread.getState());
                if (thread.getState() != Thread.State.TERMINATED){
                    statesList.add(thread.getName() + ": " + thread.getState() + "\n");
                }

            }
        }

//        for (Thread thread : threadList){
//            statesList.add(thread.getName() + ": " + thread.getState() + "\n");
//        }
//        for (String s : statesList){
//            System.out.println(textArea.getText() + "\n" + s);
//        }
        //textArea.setText(textArea.getText());
    }
}