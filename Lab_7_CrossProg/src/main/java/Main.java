import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame f=new JFrame();//creating instance of JFrame

        JButton b=new JButton("thread");//creating instance of JButton
        b.setBounds(250,300,100, 40);//x axis, y axis, width, height

        JButton b1=new JButton("executor");//creating instance of JButton
        b1.setBounds(250,400,100, 40);//x axis, y axis, width, height
        f.add(b1);

        JTextArea field = new JTextArea("Field");
        field.setBounds(0,0,200, 100);
        field.setSize(200, 500);

        JTextField threadsCount = new JTextField("2");
        JTextField density = new JTextField("100");
        threadsCount.setBounds(250,0,200, 100);
        threadsCount.setSize(100, 50);
        density.setBounds(250,100,200, 100);
        density.setSize(100, 50);
        f.add(threadsCount);
        f.add(density);


        f.add(b);//adding button in JFrame
        f.add(field);
        f.setSize(400,500);//400 width and 500 height
        f.setLayout(null);//using no layout managers
        f.setVisible(true);//making the frame visible
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Integer> results = new ArrayList<>();
                field.setText("");
                Graph graph = GraphUtils.generateGraphMatrix(5, Integer.parseInt(density.getText()));
                List<List<Integer>> g = graph.getEdges();
                field.setText(field.getText() + "\n" + "Graph:");
                for(int i = 0; i < g.size(); i++){
                    field.setText(field.getText() + "\n" + (i + 1) + ": ");
                    for (int j = 0; j < g.size(); j++) {
                        if(g.get(i).get(j) > 0){
                            field.setText(field.getText() + (j + 1)+ "(" + g.get(i).get(j) + ")" + " ");
                        }
                    }

                }
                results = runParallelMethod(graph, Integer.parseInt(threadsCount.getText()), field);
                System.out.println(results);
                field.setText(field.getText() + "\n" + results.toString());
            }
        });

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Integer> results = new ArrayList<>();
                field.setText("");
                Graph graph = GraphUtils.generateGraphMatrix(5, Integer.parseInt(density.getText()));
                List<List<Integer>> g = graph.getEdges();
                field.setText(field.getText() + "\n" + "Graph:");
                for(int i = 0; i < g.size(); i++){
                    field.setText(field.getText() + "\n" + (i + 1) + ": ");
                    for (int j = 0; j < g.size(); j++) {
                        if(g.get(i).get(j) > 0){
                            field.setText(field.getText() + (j + 1)+ "(" + g.get(i).get(j) + ")" + " ");
                        }
                    }

                }
                results = runExecutor(graph, Integer.parseInt(threadsCount.getText()), field);
                System.out.println(results);
                field.setText(field.getText() + "\n" + results.toString());
            }
        });


    }

    private static List<Integer> runParallelMethod(Graph graph, int nrThreads, JTextArea field){
        DijkstraParallel dijkstraParallel = new DijkstraParallel();
        Instant start = Instant.now();
        Monitor mon = new Monitor();
        List<Integer> results = dijkstraParallel.solve(graph, nrThreads, field, mon);
        mon.run(field);
        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println(timeElapsed);
        field.setText(field.getText() + "\n" + "Time: " + timeElapsed);
        return results;
    }

    private static List<Integer> runExecutor(Graph graph, int nrThreads, JTextArea field){
        DijkstraParallel dijkstraParallel = new DijkstraParallel();
        Instant start = Instant.now();
        Monitor mon = new Monitor();
        List<Integer> results = dijkstraParallel.solveExec(graph, nrThreads, field, mon);
        mon.run(field);
        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println(timeElapsed);
        field.setText(field.getText() + "\n" + "Time: " + timeElapsed);
        return results;
    }
}
