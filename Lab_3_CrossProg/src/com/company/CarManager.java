package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;


import static java.util.stream.Collectors.*;

public class CarManager {
    public static List<Car> cars;
    public static Map<Integer, List<String>> maxSpeeds;

    public static void readCars(String fileName) throws IOException {
        if(cars == null) {
            cars = new ArrayList<>();
        }
        List<String> carsFile = Files.readAllLines(Paths.get(fileName).toAbsolutePath());
        for (String car: carsFile) {
            List<String> carInfo = Arrays.asList(car.split("/"));
            CarManager.cars.add(new Car(carInfo.get(0), Double.parseDouble(carInfo.get(1)), Integer.parseInt(carInfo.get(2))));
        }
    }

    public static void firstTask() throws IOException {
        System.out.println("First task:");
        readCars("file1.txt");
        maxSpeeds = cars.stream().collect(groupingBy(
                Car::getMaxSpeed, mapping(Car::getModel, toList())));
        maxSpeeds.forEach((key, value) -> System.out.println(key + ": " + value));
        System.out.println();
        System.out.println("Enter count:");
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        maxSpeeds.forEach((key, value) -> System.out.println(key + ": " + Arrays.toString(value.stream().limit(count).toArray())));


    }

    public static void secondTask(){
        System.out.println();
        System.out.println("Second task:");
        List<String> userModels = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("Enter model");
            userModels.add(scanner.nextLine());
            System.out.println(userModels);
            System.out.println("Do you want add one more?(1 - no)");
            int ans = Integer.parseInt(scanner.nextLine());
            if(ans == 1){
                break;
            }
        }
        maxSpeeds.forEach((maxSpeed, models) -> models.removeIf(userModels::contains));
        System.out.println(maxSpeeds);
    }


    public static void thirdTask() throws IOException {
        System.out.println();
        System.out.println("Third task:");
        cars = null;
        readCars("file1.txt");
        List<Car> first = cars;
        cars = null;
        readCars("file2.txt");
        List<Car> second = cars;
        cars = new ArrayList<>(first);
        cars.addAll(second);
        cars.sort(Comparator.comparing(Car::getModel).reversed());
        cars.forEach(System.out::println);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter price range:");
        double start = Double.parseDouble(scanner.nextLine());
        double end = Double.parseDouble(scanner.nextLine());
        System.out.println();
        cars.stream().filter(car -> Double.compare(car.getPrice(), start) >= 0 && Double.compare(car.getPrice(), end) <= 0).forEach(System.out::println);
    }

    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Choose task:");
            int ans = Integer.parseInt(scanner.nextLine());
            if (ans == 1) {
                CarManager.firstTask();
            } else if (ans == 2) {
                CarManager.secondTask();
            } else if (ans == 3) {
                CarManager.thirdTask();
            } else {
                break;
            }
        }
    }
}
