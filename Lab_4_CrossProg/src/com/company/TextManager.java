package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.BreakIterator;
import java.util.*;

public class TextManager {
    private static List<String> sentences = new ArrayList<>();
    private static List<Integer> wordsCounts = new ArrayList<>();
    private static Map<Integer, List<String>> initials = new HashMap<>();

    public static  void splitTextToSentences(String text){
        BreakIterator bi = BreakIterator.getSentenceInstance();
        bi.setText(text);
        int index = 0;

        while (bi.next() != BreakIterator.DONE) {
            String sentence = text.substring(index, bi.current());
            sentences.add(sentence);
            index = bi.current();

        }
    }

    public static void splitSentencesToWords() {
        int index = 0;
        for (String sentence: sentences) {

            BreakIterator iterator = BreakIterator.getWordInstance();
            iterator.setText(sentence);
            int start = iterator.first();
            int count = 0;
            int findStart = -1;
            int findEnd = 0;
            int wordsCount = 0;
            for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator.next()) {
                //System.out.println(sentence.substring(start,end));
                if (Character.isLetterOrDigit(sentence.charAt(start))) {
                    wordsCount++;
                    if (Character.isUpperCase((sentence.charAt(start))) && count < 3) {

                        if (count == 0) {
                            findStart = start;
                        }
                        count++;
                        if (count == 3) {
                            System.out.println(sentence.substring(findStart, end));
                            addInitials(sentence.substring(findStart, end), index);
                        }
                    } else {
                        count = 0;
                        findStart = -1;
                    }
                }
            }
            wordsCounts.add(wordsCount);
            index++;
        }
        System.out.println();
        System.out.println(wordsCounts);
        System.out.println();
    }


    public static void addInitials(String init, int index){
       if(initials.get(index) == null){
           List<String> list = new ArrayList<String>();
           list.add(init);
           initials.put(index, list);
       } else if(!initials.get(index).contains(init)) {
           initials.get(index).add(init);
       }

    }

    public static void showSentences(){
        for (int index: initials.keySet()) {
            System.out.println("------------------------");
            if(index - 1 >= 0){
                System.out.println(sentences.get(index-1));
            }
            System.out.println(initials.get(index));
            if(index + 1 < sentences.size()){
                System.out.println(sentences.get(index+1));
            }
        }
        System.out.println("------------------------");
    }

    public static void replace(){
        for (int index: initials.keySet()) {
            for (String init: initials.get(index)) {
                sentences.set(index, sentences.get(index).replace(init, Collections.max(wordsCounts).toString()));
            }
        }
        System.out.println();
        System.out.println(String.join("", sentences));
    }

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("text.txt");
        Scanner sc = new Scanner(file);
        String text;
        StringBuilder sb = new StringBuilder();
        while (sc.hasNextLine())
            sb.append(sc.nextLine());
        text = sb.toString();
        System.out.println(text);
        System.out.println();

        //String text = "Lol Kek Cheburek tell Lol Kek Cheburek about. Work about! I Lol Kek Cheburek dont kn54554ow? sfefsefef, efsfefs545445 54455454";
        splitTextToSentences(text);
        splitSentencesToWords();
        System.out.println(initials);
        System.out.println();
        showSentences();
        replace();
    }

}
