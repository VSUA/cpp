package com.cpp.lab;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class TextManagerTest {

    @Test
    void findSentenceTest() {
        String ex = "Wddwadwdw fseffsfe. Wwdwdffrgrg!";
        assertTrue(TextManager.findSentences(ex).isEmpty());
    }

    @Test
    void findPunctuationStopsTest() {
        assertLinesMatch(List.of("fefsfefs esfeffesef", " wadwdw "),
                TextManager.findSentences("fefsfefs esfeffesef? wdadwdaw. wadwdw ?"));
    }

    @Test
    void findWordsWithLen(){
        assertEquals("There are no words of this size.", TextManager.findWordsWithLen(9, List.of("wdwdadw wdadwadawd", "wadwwad")));
    }

}
