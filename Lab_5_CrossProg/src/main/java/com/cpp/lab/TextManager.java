package com.cpp.lab;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class TextManager {

    public static String readInputText() {
        System.out.println("Enter text:");
        StringBuilder sb = new StringBuilder();
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.equals(""))
                break;
            sb.append(line);
        }

        return sb.toString();
    }


    public static List<String> findSentences(String text){
        Pattern pattern = Pattern.compile("([^.?!]*)\\?");
        List<String> sentences = new ArrayList<>();
        Matcher matcher = pattern.matcher(text);
        System.out.println();
        while (matcher.find()) {
            sentences.add(matcher.group(1));
        }
        return sentences;
    }

    public static String findWordsWithLen(int length, List<String> sentences){
        Pattern pattern = Pattern.compile("\\b\\w{"+ length +"}\\b");
        List<String> words = new ArrayList<>();
        for (String sentence:sentences) {
            Matcher matcher = pattern.matcher(sentence);
            while (matcher.find()) {
                words.add(sentence.substring(matcher.start(),matcher.end()));
            }
        }

        if(!words.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            System.out.println();
            sb.append("Words:\n");
            words.stream().collect(Collectors.toSet()).forEach(elem->{sb.append(elem); sb.append("\n");});
            return sb.toString();
        }
        return "There are no words of this size.";

    }

    public static void main(String[] args) {
//        File file = new File("text.txt");
//        Scanner sc = new Scanner(file);
//        String text;
//        StringBuilder sb = new StringBuilder();
//        while (sc.hasNextLine())
//            sb.append(sc.nextLine());
//        text = sb.toString();
//        System.out.println(text);

        List<String> sentences;
        List<String> words;

        String text = readInputText().trim();
        if (text.length() == 0){
            System.out.println("Text is empty");
            return;
        }
        sentences = findSentences(text);
        if(sentences.isEmpty()){
            System.out.println("There are no questions");
        }
        System.out.println("Questions:");
        sentences.forEach(System.out::println);

        Scanner myInput = new Scanner( System.in );
        System.out.println();
        System.out.print( "Enter length:" );
        int len = myInput.nextInt();
        System.out.println(findWordsWithLen(len, sentences));


    }
}
