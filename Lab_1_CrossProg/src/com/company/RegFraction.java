package com.company;

public class RegFraction implements Fraction{
    private long numerator;
    private long denominator;

    public RegFraction(long numerator, long denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        this.simplify();
    }

    @Override
    public String toString() {
        return numerator + " / " + denominator;
    }

    public RegFraction add(RegFraction addition) {
        return new RegFraction(numerator * addition.denominator + addition.numerator * denominator,
                denominator * addition.denominator);
    }

    private void simplify() {
        long g = gcd(numerator, denominator);
        this.numerator = numerator / g;
        this.denominator = denominator / g;
    }

    public static long gcd(long a, long b) {
        return b == 0 ? a : gcd(b, a % b);
    }
}
