package com.company;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter n: ");
        int n = scanner.nextInt();
        System.out.println("Result: " + calculate(n));

    }

    public static Fraction calculate(int n) {
        if (n <= 15) {
            RegFraction result = new RegFraction(0, 1);
            for (int i = 1; i <= n; i++) {
                result = result.add(new RegFraction(1, Math.round(Math.pow(i, 3))));
            }
            return result;
        } else {
            BigFraction result = new BigFraction(BigInteger.valueOf(0), BigInteger.valueOf(1));
            for (int i = 1; i <= n; i++) {
                result = result.add(new BigFraction(BigInteger.valueOf(1), BigInteger.valueOf(Math.round(Math.pow(i, 3)))));
            }
            return result;
        }
    }


}
