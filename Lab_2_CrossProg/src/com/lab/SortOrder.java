package com.lab;

public enum SortOrder {
    ASCENDING, DESCENDING
}
