package com.lab;

import com.lab.equipment.OfficeEquipment;
import com.lab.equipment.OfficeEquipmentShopManager;
import com.lab.equipment.impl.ElectronicEquipment;
import com.lab.equipment.impl.ScreenEquipment;

public class Demo {
    public static void main(String[] args) {
        OfficeEquipmentShopManager.initOfficeEquipmentList();
        OfficeEquipmentShopManager.addOfficeEquipment(new ElectronicEquipment("Asus", "g245", 1300.99, "M", 6));

        System.out.println("ElectronicEquipment in price range from 1000.0 to 1300.0" );
        OfficeEquipmentShopManager.printByTypeInPriceRange(ElectronicEquipment.class, 1000.0, 1300.0);

        System.out.println();
        System.out.println("ScreenEquipment in price range from 500.0 to 700.0" );
        OfficeEquipmentShopManager.printByTypeInPriceRange(ScreenEquipment.class, 500.0, 700.0);

        System.out.println();
        System.out.println("Sort list by price" );
        OfficeEquipmentShopManager.sortByPrice(SortOrder.ASCENDING);
        OfficeEquipmentShopManager.printOfficeEquipmentList();

        System.out.println();
        System.out.println("Sort list by producer" );
        OfficeEquipmentShopManager.sortByProducer(SortOrder.ASCENDING);
        OfficeEquipmentShopManager.printOfficeEquipmentList();

        System.out.println();
        System.out.println("Sort list by description" );
        OfficeEquipmentShopManager.sortByDescription(SortOrder.DESCENDING);
        OfficeEquipmentShopManager.printOfficeEquipmentList();
    }
}
