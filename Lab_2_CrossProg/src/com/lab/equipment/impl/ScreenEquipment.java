package com.lab.equipment.impl;

import com.lab.equipment.OfficeEquipment;

public class ScreenEquipment extends OfficeEquipment {
    protected Double diagonal;
    protected Integer screenHeight;
    protected Integer screenWidth;

    public ScreenEquipment(String producer, String model, Double price, String description, Double diagonal,
                           Integer screenHeight, Integer screenWidth) {
        super(producer, model, price, description);
        this.diagonal = diagonal;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
    }

    public Double getDiagonal() {
        return diagonal;
    }

    public Integer getScreenHeight() {
        return screenHeight;
    }

    public Integer getScreenWidth() {
        return screenWidth;
    }

    @Override
    public String toString() {
        return "ScreenEquipment{" +
                "producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", diagonal=" + diagonal +
                ", screenHeight=" + screenHeight + "px" +
                ", screenWidth=" + screenWidth + "px" +
                '}';
    }
}
