package com.lab.equipment.impl;

import com.lab.equipment.OfficeEquipment;

public class ElectronicEquipment extends OfficeEquipment {
    protected Integer cpuCount;

    public ElectronicEquipment(String producer, String model, Double price, String description, Integer cpuCount) {
        super(producer, model, price, description);
        this.cpuCount = cpuCount;
    }

    public Integer getCpuCount() {
        return cpuCount;
    }

    @Override
    public String toString() {
        return "ElectronicEquipment{" +
                "producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", cpuCount=" + cpuCount +
                '}';
    }
}
