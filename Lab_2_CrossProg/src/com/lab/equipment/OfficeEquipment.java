package com.lab.equipment;

public abstract class OfficeEquipment {
    protected String producer;
    protected String model;
    protected Double price;
    protected String description;

    public OfficeEquipment (String producer, String model, Double price, String description) {
        this.producer = producer;
        this.model = model;
        this.price = price;
        this.description = description;
    }

    public String getProducer() {
        return producer;
    }

    public String getModel() {
        return model;
    }

    public Double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

}
