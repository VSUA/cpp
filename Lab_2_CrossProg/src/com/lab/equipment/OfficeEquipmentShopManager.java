package com.lab.equipment;

import com.lab.SortOrder;
import com.lab.equipment.impl.ElectronicEquipment;
import com.lab.equipment.impl.ScreenEquipment;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OfficeEquipmentShopManager {
    private static List<OfficeEquipment> equipmentList;

    public static void initOfficeEquipmentList() {
        equipmentList = new ArrayList<>();
        equipmentList.add(new ElectronicEquipment("Asus", "b256", 1200.99, "A", 4));
        equipmentList.add(new ElectronicEquipment("Lenovo", "a345", 1400.99, "B", 8));
        equipmentList.add(new ScreenEquipment("Benq", "h1080", 600.0, "C", 27.0, 1080, 1920));
        equipmentList.add(new ScreenEquipment("Lenovo", "z720", 400.0, "D", 20.0, 1080, 1270));
        equipmentList.add(new ElectronicEquipment("Asus", "b256", 1500.99, "E", 6));
        equipmentList.add(new ScreenEquipment("Benq", "h720", 200.0, "F", 18.0, 1280, 720));
        equipmentList.add(new ElectronicEquipment("Lenovo", "z500", 1000.99, "G", 4));
        equipmentList.add(new ScreenEquipment("Lenovo", "4k", 1000.0, "H", 27.0, 2160, 4096));
        equipmentList.add(new ScreenEquipment("HP", "h720", 150.0, "K", 15.0, 1270, 1080));
        equipmentList.add(new ScreenEquipment("HP", "h1080", 550.0, "L", 27.0, 1080, 1920));
        equipmentList.add(new ElectronicEquipment("Asus", "g245", 1300.99, "M", 8));
        equipmentList.add(new ScreenEquipment("Asus", "h1080", 575.0, "N", 27.0, 1080, 1920));
        equipmentList.add(new ScreenEquipment("Asus", "h720", 175.0, "O", 16.0, 720, 1280));
        equipmentList.add(new ElectronicEquipment("HP", "h645", 1350.99, "P", 8));
        equipmentList.add(new ScreenEquipment("Asus", "8k", 3000.0, "Z", 27.0, 4320, 7680));
    }

    // Lambda
    public static void printByTypeInPriceRange(Class o, Double start, Double end) {
        equipmentList.stream().filter(o::isInstance).filter(x -> (x.getPrice() > start && x.getPrice() < end)).forEach(System.out::println);
    }

    //Inner static
    static class PriceComparator implements Comparator<OfficeEquipment> {

        @Override
        public int compare(OfficeEquipment o1, OfficeEquipment o2) {
            return Double.compare(o1.getPrice(), o2.getPrice());
        }
    }

    public static void sortByPrice(SortOrder order){
        if (order.equals(SortOrder.ASCENDING)){
            equipmentList.sort(new PriceComparator());
        } else {
            equipmentList.sort(new PriceComparator().reversed());
        }
    }

    //Inner
    class ProducerComparator implements Comparator<OfficeEquipment>{

        @Override
        public int compare(OfficeEquipment o1, OfficeEquipment o2) {
            return o1.getProducer().compareTo(o2.getProducer());
        }
    }

    public static void sortByProducer(SortOrder order){

        if (order.equals(SortOrder.ASCENDING)){
            equipmentList.sort(new OfficeEquipmentShopManager().new ProducerComparator());
        } else {
            equipmentList.sort(new OfficeEquipmentShopManager().new ProducerComparator().reversed());
        }
    }

    //Anon
    public static void sortByDescription(SortOrder order){
        if (order.equals(SortOrder.ASCENDING)){
            equipmentList.sort(new Comparator<OfficeEquipment>() {
                @Override
                public int compare(OfficeEquipment o1, OfficeEquipment o2) {
                    return String.CASE_INSENSITIVE_ORDER.compare(o1.getDescription(),
                            o2.getDescription());
                }
            });
        } else {
            equipmentList.sort(new Comparator<OfficeEquipment>() {
                @Override
                public int compare(OfficeEquipment o1, OfficeEquipment o2) {
                    return -String.CASE_INSENSITIVE_ORDER.compare(o1.getDescription(),
                            o2.getDescription());
                }
            });
        }
    }

    public static void addOfficeEquipment(OfficeEquipment equipment) {
        equipmentList.add(equipment);
    }

    public static void printOfficeEquipmentList() {
        equipmentList.forEach(System.out::println);
    }

}
